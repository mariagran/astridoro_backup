<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'astrid_site' );

/** MySQL database username */
define( 'DB_USER', 'astrid_root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'r@hc903@!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'iP9zI<>O;orw!A&c *0P4o&WpN1IKF8#}l~iSnz@n-GLNz}ms,8x!9J-8yf[.rci' );
define( 'SECURE_AUTH_KEY',  'idxK6~%OV3*;A$Ha)vgacg{:ws~0bZWz6=LA!q@1+G#` x/CAYg?[6>8BP^Sn@HI' );
define( 'LOGGED_IN_KEY',    'O(P j!UUoEOl<@l?n!9Ed)#5OJzE=wkMf%u,dF1O!$T,Qd_oY! %gSz9J3M:vL!4' );
define( 'NONCE_KEY',        '-f^3dx B1bXf g?.[73~0ju!W;Xzn5)K*X]+AUXXd.xwm+8>UjoM%.c37L;0$h^/' );
define( 'AUTH_SALT',        'Q?+7&%TAQ]3fkV1<a)peZC6&hMkfa%qO%o@G=BmuGphqO{Qutp?l2U2ZeSgYmn3L' );
define( 'SECURE_AUTH_SALT', 'aQK@S7/S{Q`UPE-VhI;LS%qxK#2l%jO5EaE kjtTq]G~/mxfU`C/f;37jP(!OTT%' );
define( 'LOGGED_IN_SALT',   'e`~Z96t3K?4.`pd0b#C-poZ2n}PgkwnDJ$?,$2~5:8nt/pE38K/M0B,M`&Op3-Tq' );
define( 'NONCE_SALT',       'K+h>vb@Jc0Pl&$YuR/2hwv- uWKR)JL)!Yw@p(DoScj<ha5_[!6.G,zy#b!`2#$c' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'at_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
