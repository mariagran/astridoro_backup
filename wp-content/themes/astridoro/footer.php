<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astrid\'Oro
 */

global $configuracao;

?>

	<footer>
		<div class="full-container">
			<div class="row">
				<div class="col-sm-3">
					<figure class="logo">
						<img src="<?= get_template_directory_uri(); ?>/img/logo-footer.png" alt="Logo">
					</figure>
				</div>
				<div class="col-sm-2">
					<div class="menu-footer">
						<h4>Informações</h4>
						<nav>
							<a href="<?= home_url('/quem-somos/') ?>">A empresa</a>
							<a href="<?= home_url('/suporte/') ?>">Suporte / FAQ</a>
							<a href="<?= home_url('/orcamento/') ?>">Fazer pedido</a>
							<a href="<?= home_url('#contato') ?>">Trabalhe conosco</a>
						</nav>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="menu-footer newsletter">
						<h4>Newsletter</h4>
						<!-- <form>
							<input type="text" placeholder="Informe seu endereço de email...">
							<input type="submit" value="Inscrever">
						</form> -->
						<?= do_shortcode('[wysija_form id="1"]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="copyright">
			<p>Copyright ₢ 2018 Astrid'Oro - Direitos reservados.</p>
			<p>Desenvolvido por <a href="https://handgran.com/">Handgran</a></p>
		</div>
	</footer>

	<script src="<?= get_template_directory_uri() . '/js/jquery.min.js' ?>"></script>
	<script src="<?= get_template_directory_uri() . '/js/bootstrap.min.js' ?>"></script>
	<script src="<?= get_template_directory_uri() . '/slick/slick.min.js' ?>"></script>
	<script src="<?= get_template_directory_uri() . '/js/geral.js' ?>"></script>

	<?php wp_footer(); ?>
</body>
</html>