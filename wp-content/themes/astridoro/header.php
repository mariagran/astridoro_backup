<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astrid\'Oro
 */

global $configuracao;

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?= get_template_directory_uri() . '/img/favicon.ico' ?>" /> 

	<script>
	(function(h,e,a,t,m,p) {
	m=e.createElement(a);m.async=!0;m.src=t;
	p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
	})(window,document,'script','https://u.heatmap.it/log.js');
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<!-- TOPO -->
	<header class="header-desktop">
		<div class="smaller-nav">
			<div class="full-container">
				<div class="row">
					<div class="col-sm-6">
						<div class="smaller-nav-info atendimento">
							<p>Seg - Qui: 8:30 - 18:00 | Sex: 8:30 - 17:00 / R. do Pinheiro, 665 - Colombo - PR</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="smaller-nav-info contato">
							<p>astridoro@astridoro.com.br<small>/</small>(41) 3656-3333<small>/</small></p>
							<a href="https://web.facebook.com/astridoro?_rdc=1&_rdr"><img src="<?= get_template_directory_uri() . '/img/fb.svg' ?>" alt="Facebook"></a>
						</div>
					</div>
				</div>	
			</div>
		</div>
		<div class="destaque">
			<div class="full-container">
				<div class="row">
					<a href="<?= home_url(); ?>" class="logo">
						<figure>
							<img class="img-responsive" src="<?= get_template_directory_uri() . '/img/logo.png' ?>" alt="Logo Astridoro">
						</figure>
					</a>
					<div class="titulo-destaque">
						<h1 class="titulo">Metais para brindes e bijouterias</h1>
						<p>Fundição e Estamparia</p>
					</div>
				</div>
			</div>
		</div>
		<div class="navbar" role="navigation">	
			<div class="full-container">
				<!-- MENU MOBILE TRIGGER -->
				<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
					<span class="sr-only">Menu</span>
				</button>
				<!--  MENU MOBILE-->
				<div class="row navbar-header">			
					<nav class="collapse navbar-collapse" id="collapse">
						<ul class="nav navbar-nav">			
							<li><a href="<?= home_url() ?>">Principal</a></li>
							<li><a href="<?= home_url('/quem-somos/') ?>">Quem somos</a></li>
							<li><a href="<?= home_url('/produtos/') ?>">Produtos</a></li>
							<li><a href="<?= home_url() ?>"><img src="<?= get_template_directory_uri() . '/img/sol-menu.png' ?>" alt=""></a></li>
							<li><a href="<?= home_url('/orcamento/') ?>">Orçamento</a></li>
							<li><a href="#contato">Contato</a></li>
							<li><a href="<?= home_url('/suporte/') ?>">Suporte</a></li>
						</ul>
					</nav>						
				</div>			
			</div>
		</div>
	</header>

	<header class="header-mobile">
		<div class="full-container">
			<div class="row">
				<div class="col-sm-3">
					<a href="<?= home_url(); ?>" class="logo">
						<figure>
							<img class="img-responsive" src="<?= get_template_directory_uri() . '/img/logo.png' ?>" alt="Logo Astridoro">
						</figure>
					</a>
				</div>
				<div class="col-sm-9">
					<div class="navbar" role="navigation">	
						<!-- MENU MOBILE TRIGGER -->
						<button type="button" id="botao-menu" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-header-mobile">
							<span class="sr-only"></span>
							<span></span>
							<span></span>
							<span></span>
						</button>
						<!--  MENU MOBILE-->
						<div class="row navbar-header">			
							<nav class="collapse navbar-collapse" id="collapse-header-mobile">
								<ul class="nav navbar-nav">			
									<li><a href="<?= home_url() ?>">Principal</a></li>
									<li><a href="<?= home_url('/quem-somos/') ?>">Quem somos</a></li>
									<li><a href="<?= home_url('/produtos/') ?>">Produtos</a></li>
									<li><a href="<?= home_url('/suporte/') ?>">Suporte</a></li>
									<li><a href="<?= home_url() ?>">Loja astrid'oro bijoux</a></li>
									<li><a href="<?= home_url('/orcamento/') ?>">Orçamento</a></li>
									<li><a href="#contato">Contato</a></li>
								</ul>
							</nav>						
						</div>			
					</div>
				</div>
			</div>
		</div>
		<div class="destaque"></div>
	</header>