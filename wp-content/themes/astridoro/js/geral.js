(function(){
	$(document).ready(function(){
		if(document.location.href == document.location.origin || document.location.href == document.location.origin + '/' || document.location.href == document.location.origin + '/#' || document.location.href == document.location.origin + '/#contato'){
			$('.carrossel-clientes').slick({
				dots: false,
				speed: 450,
				slidesToShow: 5,
				slidesToScroll: 1,
				autoplay: false,
				autoplaySpeed: 2000,
				arrows: false,
				responsive: [
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 425,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
				// You can unslick at a given breakpoint now by adding:
				// settings: "unslick"
				// instead of a settings object
				]
			});
		}
	});

	$('.pg-produtos .pop-up-produto span.close-button').click(function(){
		$('.pg-produtos .pop-up .pop-up-produto').removeClass('show-pop-up');
		$('.pg-produtos .pop-up .pop-up-produto').removeClass('open-pop-up');
		setTimeout(function(){
			$('.pg-produtos .pop-up').removeClass('open-pop-up');
			$('.pg-produtos .pop-up').removeClass('show-pop-up');
		}, 500);
		setTimeout(function(){
			$('body').removeClass('stop-scroll');
		}, 1000);
	});

	$('.pg-produtos .secao-produtos .lista-produtos li figure img').click(function(){

		const dataImg = $(this).attr('data-img');
		console.log(dataImg);
		$('.pg-produtos .pop-up-produto figure img').attr('src', dataImg);

		$('.pg-produtos .pop-up').addClass('open-pop-up');
		$('.pg-produtos .pop-up .pop-up-produto').addClass('open-pop-up');
		setTimeout(function(){
			$('.pg-produtos .pop-up').addClass('show-pop-up');
			$('.pg-produtos .pop-up .pop-up-produto').addClass('show-pop-up');
		}, 500);
		setTimeout(function(){
			$('body').addClass('stop-scroll');
		}, 1000);
	});

	$('.pg-produtos .filtro-produtos a').click(function(e){
		e.preventDefault();
		let filtroId = $(this).attr('id');
		$('.pg-produtos .secao-produtos .lista-produtos').removeClass('lista-ativa');
		$('.pg-produtos .secao-produtos ul#'+filtroId+'.lista-produtos').addClass('lista-ativa');
	});

}());