<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Astrid\'Oro
 */

get_header();

?>

<div class="pg pg-inicial">
	<section class="secao-brindes">
		<h2 class="hidden">SEÇÃO DE BRINDES</h2>
		<div class="full-container">
			<h2 class="titulo">brindes personalizados</h2>
			<ul class="lista-brindes">
				<li>
					<figure>
						<img src="<?= get_template_directory_uri() . '/img/b1.jpg' ?>" alt="Brinde">
						<figcaption class="hidden">Brinde</figcaption>
					</figure>
					<h2>bottons & pins de lapela</h2>
					<p>Utilizados na divulgação de campanhas publicitárias, eventos e premiações. Podem ser esmaltados à mão ou adesivados, sobre o metal fundido. Conhecidos também como "alfinetes de labela".</p>
				</li>
				<li>
					<figure>
						<img src="<?= get_template_directory_uri() . '/img/b2.jpg' ?>" alt="Brinde">
						<figcaption class="hidden">Brinde</figcaption>
					</figure>
					<h2>clips institucionais exclusivos</h2>
					<p>Exclusividade Astridoro. Concebido para ser utilizado como marcador de compromissos em agenda, oeganizador de documentos e afins.</p>
				</li>
				<li>
					<figure>
						<img src="<?= get_template_directory_uri() . '/img/b3.jpg' ?>" alt="Brinde">
						<figcaption class="hidden">Brinde</figcaption>
					</figure>
					<h2>medalhas, comenda & medalhões</h2>
					<p>São objetos de maior tamanho, volume e peso. Podem ter fita de sustentação ou ser entregues acondicionados em estojos com "berço".</p>
				</li>
				<li>
					<figure>
						<img src="<?= get_template_directory_uri() . '/img/b4.jpg' ?>" alt="Brinde">
						<figcaption class="hidden">Brinde</figcaption>
					</figure>
					<h2>chaveiros, crachás e insígnias</h2>
					<p>Contém, invariavelmente, a identidade visual do contratante e são utilizados em uniformes, carteira de identificação profissional, etc.</p>
				</li>
			</ul>
		</div>
	</section>
	<div class="solicite-orcamento">
		<div class="full-container">
			<h5>Quer mais informações, solicite agora um orçamento personalizado</h5>
			<a href="<?= home_url('/orcamento/'); ?>" class="btn-orcamento">Orçamento</a>
		</div>
	</div>
	<section class="secao-empresa">
		<h2 class="hidden">SEÇÃO SOBRE A EMPRESA</h2>
		<div class="full-container">
			<h2 class="titulo">a empresa</h2>
			<div class="conteudo-empresa">
				<p>Desde 1986 a Astrid'oro materializa marcas e idéias. com fabricação própria e vendas sob encomenda, possui tecnologia, experiência e comprometimento com exigências como qualidade, prazos e preços. A empresa sente-se hoje preparada para atender a todo o mercado nacional e/ou Mercosul em todas as suas demandas por todo o leque de produtos nos segmentos de Acessórios de Moda e Comunicação Publicitária.</p>
				<figure>
					<img src="<?= get_template_directory_uri() . '/img/selo.png' ?>" alt="Selo Astridoro">
				</figure>
			</div>
		</div>
	</section>
	<section class="secao-acabamentos">
		<h2 class="hidden">SEÇÃO DE ACABAMENTOS</h2>
		<div class="full-container">
			<div class="row">
				<div class="col-sm-6">
					<figure>
						<img src="<?= get_template_directory_uri() . '/img/acabamentos.jpg' ?>" alt="Acabamentos">
					</figure>
				</div>
				<div class="col-sm-6">
					<div class="conteudo-acabamento">
						<h2 class="titulo">Acabamentos</h2>
						<ul>
							<li>dourado</li>
							<li><img src="<?= get_template_directory_uri() . '/img/trofeu.png' ?>" alt="Acabamento"></li>
							<li>prateado</li>
							<li>grafite</li>
							<li><img src="<?= get_template_directory_uri() . '/img/medalha.png' ?>" alt="Acabamento"></li>
							<li>cobre</li>
							<li>fumê</li>
							<li><img src="<?= get_template_directory_uri() . '/img/broche.png' ?>" alt="Acabamento"></li>
							<li>latão</li>
						</ul>
						<p>Todos nas opções</p>
						<p>brilhante<small>|</small>fosco<small>|</small>envelhecido</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="secao-clientes">
		<h2 class="hidden">SEÇÃO DE CLIENTES</h2>
		<div class="full-container">
			<h2 class="titulo">clientes</h2>
			<ul class="lista-clientes carrossel-clientes">
				<li><img src="<?= get_template_directory_uri() . '/img/1.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/2.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/3.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/4.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/5.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/6.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/7.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/8.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/9.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/10.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/11.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/12.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/13.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/14.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/15.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/16.jpg'; ?>" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri() . '/img/17.jpg'; ?>" alt="Cliente astridoro"></li>
			</ul>
		</div>
	</section>
	<div class="fundicao-artistica">
		<h5 class="titulo">astridoro fundição artística de metais</h5>
	</div>
	<section class="secao-contato" id="contato">
		<h2 class="hidden">SEÇÃO ENTRE EM CONTATO</h2>
		<div class="full-container">
			<h3 class="titulo">Entre em contato</h3>
			<div class="row">
				<div class="col-sm-5">
					<div class="informacoes-contato">
						<p>endereço</p>
						<h4>Rua do Pinheiro, 665<br>Parque do Embú, Colombo - PR<br>CEP: 83414-570</h4>
						<p>Telefone</p>
						<h4><a href="tel:4136563333">(41) 3656-3333</a></h4>
						<p>Email</p>
						<h4><a href="mailto:astridoro@astridoro.com.br">astridoro@astridoro.com.br</a></h4>
						<a href="https://web.facebook.com/astridoro?_rdc=1&_rdr"><img src="<?= get_template_directory_uri() . '/img/ico-f.png' ?>" alt="Facebook"></a>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="formulario-contato">
						<!-- <form>
							<input type="text" name="campo-nome" id="campo-nome" class="campo-nome" placeholder="Nome">
							<input type="email" name="campo-email" id="campo-email" class="campo-email" placeholder="Email">
							<textarea name="campo-mensagem" id="campo-mensagem" class="campo-mensagem" placeholder="Mensagem"></textarea>
							<input type="submit" value="Enviar mensagem">
						</form> -->
						<?= do_shortcode('[contact-form-7 id="5" title="Fomulário de contato"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="mapa" id="mapa">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3606.9165993689553!2d-49.214456!3d-25.307006!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce6fafbb907ed%3A0x329007bf79bb4a74!2sAstridoro%20Fundi%C3%A7%C3%A3o%20Art%C3%ADstica%20de%20Metais!5e0!3m2!1spt-BR!2sus!4v1574273895983!5m2!1spt-BR!2sus" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	</div>
</div>

<?php get_footer();