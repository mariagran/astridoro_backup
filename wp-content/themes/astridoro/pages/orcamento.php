<?php

/**
 * Template Name: Orçamento
 * Description: Página Orçamento
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Astrid\'Oro
 */

get_header('secundaria');

?>

<div class="pg pg-orcamento">
	<div class="container">
		<div class="row">
			<div class="col-sm-5">
				<div class="informacoes-contato">
					<figure class="logo">
						<img src="<?= get_template_directory_uri(); ?>/img/logo.png" alt="Logo">
					</figure>
					<p>endereço</p>
					<h4>Rua do Pinheiro, 665<br>Parque do Embú, Colombo - PR<br>CEP: 83414-570</h4>
					<p>Telefone</p>
					<h4><a href="tel:4136563333">(41) 3656-3333</a></h4>
					<p>Email</p>
					<h4><a href="mailto:astridoro@astridoro.com.br">astridoro@astridoro.com.br</a></h4>
					<p>Redes sociais</p>
					<a href="https://web.facebook.com/astridoro?_rdc=1&_rdr"><img src="<?= get_template_directory_uri(); ?>/img/ico-f-2.png" alt="Facebook"></a>
				</div>
			</div>
			<div class="col-sm-7">
				<div class="formulario-orcamento formulario-contato">
					<h2>Dados para orçamento</h2>
					<p>Preencha os campos abaixo e solicite um orçamento sem compromisso!</p>
					<!-- <form>
						<input type="text" name="nome" id="nome" class="nome" placeholder="Nome">
						<input type="text" name="empresa" id="empresa" class="empresa" placeholder="Empresa">
						<input type="text" name="cidade" id="cidade" class="cidade" placeholder="Cidade">
						<select name="estado" id="estado">
							<option value="">Estado</option>
							<option value="Paraná">Paraná</option>
							<option value="São Paulo">São Paulo</option>
							<option value="Santa Catarina">Santa Catarina</option>
							<option value="Rio Grande do Sul">Rio Grande do Sul</option>
						</select>
						<input type="tel" name="telefone" id="telefone" class="telefone" placeholder="Fone">
						<input type="tel" name="celular" id="celular" class="celular" placeholder="Celular">
						<input type="email" name="email" id="email" class="email" placeholder="Email">
						<div class="checkboxes">
							<label for="pin">
								<input type="checkbox" id="pin" name="checkbox-produto">
								<span>Pins</span>
							</label>
							<label for="botton">
								<input type="checkbox" id="botton" name="checkbox-produto">
								<span>Bottons</span>
							</label>
							<label for="clips">
								<input type="checkbox" id="clips" name="checkbox-produto">
								<span>Clips</span>
							</label>
							<label for="chaveiro">
								<input type="checkbox" id="chaveiro" name="checkbox-produto">
								<span>Chaveiros</span>
							</label>
							<label for="cracha">
								<input type="checkbox" id="cracha" name="checkbox-produto">
								<span>Crachás</span>
							</label>
							<label for="insignia">
								<input type="checkbox" id="insignia" name="checkbox-produto">
								<span>Insígnias</span>
							</label>
							<label for="medalha">
								<input type="checkbox" id="medalha" name="checkbox-produto">
								<span>Medalhas</span>
							</label>
							<label for="comenda">
								<input type="checkbox" id="comenda" name="checkbox-produto">
								<span>Comendas</span>
							</label>
							<label for="placa">
								<input type="checkbox" id="placa" name="checkbox-produto">
								<span>Placas</span>
							</label>
							<label for="trofeu">
								<input type="checkbox" id="trofeu" name="checkbox-produto">
								<span>Troféus</span>
							</label>
							<label for="bandeira">
								<input type="checkbox" id="bandeira" name="checkbox-produto">
								<span>Bandeiras</span>
							</label>
							<label for="outro">
								<input type="checkbox" id="outro" name="checkbox-produto">
								<span>Outros</span>
							</label>
						</div>
						<textarea name="observacao" id="observacao" placeholder="Observações"></textarea>
						<input type="submit" value="Enviar mensagem">
					</form> -->
					<?= do_shortcode('[contact-form-7 id="7" title="Formulário de orçamento"]'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="fundicao-artistica">
		<h5 class="titulo">astridoro fundição artística de metais</h5>
	</div>
</div>

<?php get_footer();