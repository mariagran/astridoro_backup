<?php

/**
 * Template Name: Produtos
 * Description: Página Produtos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Astrid\'Oro
 */

get_header('secundaria');

?>

<div class="pg pg-produtos">
	<div class="filtro-produtos">
		<div class="container">
			<a href="#" id="0">Todos</a>
			<ul>
				<li><a href="#" id="1">Pins</a></li>
				<li><a href="#" id="2">Bottons</a></li>
				<li><a href="#" id="3">Clips</a></li>
				<li><a href="#" id="4">Chaveiros</a></li>
				<li><a href="#" id="5">Crachás</a></li>
				<li><a href="#" id="6">Insígnias</a></li>
				<li><a href="#" id="7">Medalhas</a></li>
				<li><a href="#" id="8">Comendas</a></li>
				<li><a href="#" id="9">Placas</a></li>
				<li><a href="#" id="10">Troféus</a></li>
				<li><a href="#" id="11">Bandeiras</a></li>
			</ul>
		</div>
	</div>
	<section class="secao-produtos">
		<div class="container">
			<ul class="lista-produtos lista-ativa" id="0">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/20.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/20-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/06.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/06-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/07.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/07-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/08.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/08-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/09.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/09-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/10.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/10-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/11.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/11-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/12.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/12-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/13.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/13-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/14.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/14-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/15.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/15-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/16.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/16-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/17.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/17-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/18.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/18-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/19.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/19-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/cartepilar.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/cartepilar-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/dr-ricardo.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/dr-ricardo-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Merito-Domingos-de-Brito.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Merito-Domingos-de-Brito-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Cipa.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Cipa-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/brigada-de-incendio.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/brigada-de-incendio-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Feminina.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Feminina-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Copa.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Copa-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-brasil.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-brasil-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-paraná-original.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-paraná-original-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-santa-catarina.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-santa-catarina-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-rio-grande-do-sul.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-rio-grande-do-sul-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-personalizada.gif" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-personalizada.gif" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/steward-e-sons.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/steward-e-sons-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Volutarios-do-Bem.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Volutarios-do-Bem-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/05-sanepar.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/05-sanepar-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar-caixinha.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar-caixinha-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01-caixinha.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01-caixinha-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/crachá-site.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/crachá-site-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/trofeu-personalizado.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/trofeu-personalizado-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-25-mm.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-25-mm-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-35-mm.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-35-mm-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-45-mm.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-45-mm-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais__03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais__03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_036.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_036-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_034.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_034-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_035.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_035-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_033.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_033-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_032.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_032-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_031.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_031-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_030.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_030-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_029.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_029-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_022.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_022-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_023.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_023-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_024.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_024-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_025.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_025-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_026.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_026-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_027.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_027-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_028.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_028-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_021.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_021-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_020.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_020-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_019.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_019-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_018.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_018-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_017.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_017-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_016.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_016-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_015.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_015-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_014.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_014-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_013.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_013-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_012.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_012-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_011.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_011-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_010.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_010-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_09.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_09-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_08.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_08-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_07.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_07-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_06_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_06_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_07_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_07_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/pindelapela_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/pindelapela_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_06.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_06-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_05_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_05_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_03_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_03_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/cracha_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/cracha_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_02-1.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_02-1-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="1">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/20.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/20-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/06.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/06-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/07.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/07-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/08.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/08-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/09.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/09-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/10.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/10-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/11.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/11-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/12.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/12-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/13.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/13-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/14.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/14-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/15.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/15-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/16.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/16-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/17.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/17-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/18.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/18-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/19.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/19-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Cipa.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Cipa-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/brigada-de-incendio.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/brigada-de-incendio-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Feminina.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Feminina-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/pin-copa-2018-mod-01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/steward-e-sons.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/steward-e-sons-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Volutarios-do-Bem.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Volutarios-do-Bem-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/05-sanepar.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/05-sanepar-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar-caixinha.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/40-sanepar-caixinha-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01-caixinha.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/todas_01-caixinha-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>

				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_036.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_036-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_034.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_034-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_035.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_035-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_033.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_033-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_032.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_032-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_031.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_031-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_030.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_030-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_029.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_029-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_022.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_022-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_023.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_023-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_024.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_024-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_025.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_025-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_026.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_026-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_027.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_027-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_028.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_028-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_021.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_021-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_020.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_020-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_019.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_019-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_018.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_018-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_017.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_017-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_016.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_016-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_015.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_015-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_014.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_014-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_013.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_013-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_012.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_012-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_011.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_011-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_010.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_010-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_09.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_09-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_08.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_08-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_07.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_07-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_06_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_06_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottons_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="2">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-25-mm.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-25-mm-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-35-mm.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-35-mm-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-45-mm.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bottom-45-mm-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="3">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais__03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais__03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/clips_institucionais_05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="4">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Copa.png" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Copa-270x193.png" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_05.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_05-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_02-1.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_02-1-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/chaveiro_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="5">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/crachá-site.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/crachá-site-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/cracha_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/cracha_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="6">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_07_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_07_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_06.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_06-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_05_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_05_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_03_.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_03_-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/insignea_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="7">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/Merito-Domingos-de-Brito.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/Merito-Domingos-de-Brito-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/medalha_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="8">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_01.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_01-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_04.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_04-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_03.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_03-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_02.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/comenda_02-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="9">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/cartepilar.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/cartepilar-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/dr-ricardo.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/dr-ricardo-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="10">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/trofeu-personalizado.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/trofeu-personalizado-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
			<ul class="lista-produtos" id="11">
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-brasil.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-brasil-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-paraná-original.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-paraná-original-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-santa-catarina.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-santa-catarina-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-rio-grande-do-sul.jpg" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-rio-grande-do-sul-270x193.jpg" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
				<li>
					<figure>
						<img data-img="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-personalizada.gif" src="http://astridoro.gran.group/wp-content/uploads/2019/11/bandeira-personalizada.gif" alt="Produto">
						<figcaption class="hidden">Produto</figcaption>
					</figure>
				</li>
			</ul>
		</div>
	</section>
	<div class="fundicao-artistica">
		<h5 class="titulo">astridoro fundição artística de metais</h5>
	</div>

	<div class="pop-up">
		<div class="pop-up-produto">
			<span class="close-button"></span>
			<figure>
				<img src="http://astridoro.gran.group/wp-content/uploads/2019/11/05-sanepar-150x150-1.png" alt="Produto">
				<figcaption class="hidden">Produto</figcaption>
			</figure>
			<p>Faça seu PIN personalizado!</p>
			<a href="<?= home_url('/orcamento/'); ?>">Orçamento</a>
		</div>
	</div>
</div>

<?php get_footer();