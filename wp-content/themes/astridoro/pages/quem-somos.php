<?php

/**
 * Template Name: Quem somos
 * Description: Página Quem somos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Astrid\'Oro
 */

get_header('secundaria');

?>

<div class="pg pg-quem-somos">
	<section class="secao-historia secao-texto">
		<h2 class="hidden">SEÇÃO HISTÓRIA</h2>
		<div class="container">
			<div class="quem-somos">
				<h2 class="titulo titulo-secundario">história</h2>
				<div class="row">
					<div class="col-sm-6">
						<article>
							<p>Em 1985 (2º semestre) iniciamos nosso empreendimento autônomo e independente, ainda informal, criando, produzindo e comercializando artesanato com motivos natalinos, em especial enfeites para decoração de árvores e ambientes.</p>
							<p>Em 1986 constituímos formalmente o negócio e é nesta época que obtivemos o nosso primeiro contrato social. Passamos a ser uma Micro Empresa oficial!</p>
							<p>Com a experiência adquirida nas pinturas e artesanatos natalinos fomos buscar mais informações nos maiores centros de comércio popular do País, quais sejam: a região da Rua 25 de Março, Ladeira Porto Geral e Bráz em São Paulo (capital) e Senhor dos Passos no Rio de Janeiro.</p>
							<p>Conhecer esse novo universo nos estimulou a abrir, em Curitiba, uma pequena loja na região central, voltada ao comércio de componentes e peças para montagem de bijuterias e afins. A idéia era aproximar os produtos dos consumidores, diminuindo assim a dependência do eixo Rio-São Paulo e as distâncias entre o mercado produtor e consumidor.</p>
							<p>A criação da primeira loja de Curitiba especializada neste segmento, nos levou a participar de muitos cursos e work shops e nesta direção, passamos também a oferecer cursos para montagens. Não nos limitamos ao que aprendemos fora.</p>
							<p>Nossos cursos foram além, oferecendo apostilas para estudo com croquis de montagens, dicas sobre técnicas manuais, materiais, recomendações e lembretes preciosos para os alunos. Ao final os participantes recebiam Certificados de Conclusão, conforme o conteúdo escolhido e praticado.</p>
							<p>Até 1990 continuamos na condição de revendedores de peças e componentes. Paralelamente à revenda, desenvolvíamos nossos próprios modelos que eram oferecidos/comercializados totalmente com o mercado de atacado de São Paulo. É</p>
						</article>
					</div>
					<div class="col-sm-6">
						<article>
							<p>deste tempo que vem nosso conhecimento de grandes nomes e parcerias. São estes parceiros que nos estimularam e provocaram para que além de revendedores, passássemos a produzir e colocar à venda peças e componentes idealizados por nós.</p>
							<p>Isto nos levou a partir de 1991 ao início da fabricação própria, com design exclusivo de itens (brincos, colares, pingentes, entremeios e afins) elaborados em metal fundido (as chamadas ligas de baixa fusão) pelo método da centrifugação sob pressão.</p>
						</article>
						<figure>
							<img src="<?= get_template_directory_uri(); ?>/img/ASTRID_AEREA-300x199.jpg" alt="Vista aérea da astridoro">
							<figcaption>Vista aérea da Astrid’Oro</figcaption>
						</figure>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="secao-fabrica secao-texto">
		<h2 class="hidden">SEÇÃO SOBRE A FÁBRICA</h2>
		<div class="container">
			<div class="quem-somos">
				<h2 class="titulo titulo-secundario">fábrica de bijus</h2>
				<div class="row">
					<div class="col-sm-6">
						<article>
							<p>Nossas primeiras experiências com fundição de metais e os processos de transformação de barras de liga metálica em produtos acabados, foram realizadas para produzir componentes para venda na própria loja. Logo as demandas externas nos deixaram claro a impossibilidade de conciliar a revenda de terceiros com a produção e comercialização de modelos próprios.</p>
							<p>É desta época a mais rica e perene parceria da Astridoro até os dias atuais, o que nos permite oferecer no segmento dos acessórios femininos, ineditismo e exclusividade na produção de coleções de anéis, brincos, broches, colares, conjuntos, presilhas, pulseiras e tantas outras.</p>
							<p>Nossa parceria internacional nos assegura acesso a matrizes e protótipos oriundos diretamente de escolas de artífices na Itália, considerada como berço mundial da modelagem.</p>
							<p>É assim, que com muita dedicação, desenvolvendo idéias e buscando as melhores soluções para nossos clientes, que nos transformamos em fornecedores de componentes e bijuterias prontas para lojas especializadas, especialmente nas regiões Sul e Sudeste do Brasil. São desta época: Laskani, Moreleh, Carla Prado, Brilho´s, Mutti, Kálice Jóias entre outros.</p>
						</article>
					</div>
					<div class="col-sm-6">
						<article>
							<p>A entrada de produtos oriundos da China é um divisor de águas no mercado nacional de produtos no segmento de bijuterias e assemelhados. A partir da invasão de produtos chineses, reconhecidamente de baixa qualidade, contudo de baixo custo, as indústrias nacionais passaram a enfrentar dificuldades quase insuperáveis para colocar no mercado as produções internas. Como forma de equilibrar e sustentar a estrutura de produção, a Astridoro diversificou sua atuação e passou a fornecer produtos “em bruto” para o comércio de semi-jóias e também ao ramo da comunicação publicitária, envolvendo: Pins, bottons, prendedores de gravata, medalhas, chaveiros, brasões, comendas entre outros.</p>
							<p>No ano 2000 implantamos em Colombo – Pr. nossa planta industrial contida em um terreno de 1.000 m2 , com uma construção de 350 m2, especialmente projetada  para cada etapa do processo de produção, com ambientes específicos desde a fundição até os processos de tratamento de superfície, passando por um competente trabalho de acabamento (esmaltação colorida,colagens de pedras e strass, montagens, embalagem e expedição).</p>
						</article>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="secao-cliente">
		<h2 class="hidden">SEÇÃO CLIENTES</h2>
		<div class="container">
			<div class="quem-somos">
				<h2 class="titulo titulo-secundario">clientes</h2>
			</div>
			<ul class="lista-clientes">
				<li><img src="<?= get_template_directory_uri(); ?>/img/8.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/9.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/10.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/11.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/12.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/13.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/14.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/15.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/16.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/17.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/1.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/2.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/3.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/4.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/5.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/6.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/18.jpg" alt="Cliente astridoro"></li>
				<li><img src="<?= get_template_directory_uri(); ?>/img/7.jpg" alt="Cliente astridoro"></li>
			</ul>
		</div>
	</section>
	<div class="entregas">
		<div class="container">
			<div class="quem-somos">
				<div class="row">
					<div class="col-sm-8">
						<figure>
							<img src="<?= get_template_directory_uri(); ?>/img/32-anos_astrid_25milhao.jpg" alt="Selo astridoro">
						</figure>
					</div>
					<div class="col-sm-4">
						<ul>
							<li>Materializamos suas marcas e ideias;</li>
							<li>Oferecemos Preços e Prazos atraentes;</li>
							<li>Fabricação própria e vendas sob encomenda;</li>
							<li>Atendimento diferenciado;</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="section-sobre-empresa">
		<h2 class="hidden">SEÇÃO
		 SOBRE A EMPRESA</h2>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="topico">
						<h4 class="titulo">missão</h4>
						<p>Garantir, nos contratos de negócio, que seus maiores talentos sejam a excelência no atendimento, o rigoroso cumprimento de prazos, a busca constante da inovação e a perfeita harmonia e confiança entre clientes, fornecedores e colaboradores.</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="topico">
						<h4 class="titulo">visão</h4>
						<p>Ser reconhecida no âmbito regional e nacional, como uma empresa focada em entregar aos seus clientes, produtos referência nos segmentos onde atua.</p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="topico">
						<h4 class="titulo">valores</h4>
						<p>Prestar, junto a cada cliente, um serviço confiável de alta qualidade a partir do conhecimento sobre as demandas a serem satisfeitas, com o objetivo de preservar e fazer prosperar, relações comerciais claras e honestas, de curto e longo prazo, mantendo disposição permanente para melhorar ou criar produtos, processos ou serviços.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="fundicao-artistica">
		<h5 class="titulo">astridoro fundição artística de metais</h5>
	</div>
</div>

<?php get_footer();