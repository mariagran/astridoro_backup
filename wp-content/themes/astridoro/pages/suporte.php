<?php

/**
 * Template Name: Suporte
 * Description: Página Suporte
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Astrid\'Oro
 */

get_header('secundaria');

?>

<div class="pg pg-suporte">
	<div class="small-container">
		<h2 class="titulo titulo-secundario">suporte</h2>
		<h4>entendendo o processo</h4>
		<p>O processo de fundição das peças inicia-se com a confecção de clichês elaborados com base nos fotolitos gerados em bureaus especializados. Portanto, todas os produtos são cuidadosamente redesenhados em programas de desenho vetorial, com base nos originais fornecidos pelo cliente.</p>
		<h4>enviando suas imagens</h4>
		<p>Para que as peças saiam perfeitas e a qualidade impecável, é preciso que a logomarca enviada, contenha a melhor resolução possível, com traços, cores e contornos bem definidos. Use preferencialmente arquivos no programa CORELDRAW vetorizado. Com a imagem em mãos, faremos amostras virtuais para aprovação. Nesta fase serão feitas as modificações necessárias, se for o caso (alteração de tamanho por exemplo).</p>
		<p>Caso não tenha o desenho neste formato, será preciso redesenhar a arte e isto pode gerar um custo adicional. Neste caso, use preferencialmente arquivos JPG, PDF ou ADOBE ILLUSTRATOR.</p>
	</div>
	<div class="solicite-orcamento">
		<div class="full-container">
			<h5>Quer mais informações, solicite agora um orçamento personalizado</h5>
			<a href="<?= home_url('/orcamento/'); ?>" class="btn-orcamento">Orçamento</a>
		</div>
	</div>
	<div class="fundicao-artistica">
		<h5 class="titulo">astridoro fundição artística de metais</h5>
	</div>
</div>

<?php get_footer();